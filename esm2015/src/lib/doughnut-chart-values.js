/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/doughnut-chart-values.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function DoughnutChartValues() { }
if (false) {
    /** @type {?} */
    DoughnutChartValues.prototype.doughnutChartColors;
    /** @type {?} */
    DoughnutChartValues.prototype.doughnutChartLabels;
    /** @type {?} */
    DoughnutChartValues.prototype.doughnutChartData;
    /** @type {?} */
    DoughnutChartValues.prototype.chartLabel;
    /** @type {?} */
    DoughnutChartValues.prototype.insideChartLabel;
    /** @type {?} */
    DoughnutChartValues.prototype.insideChartNumber;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG91Z2hudXQtY2hhcnQtdmFsdWVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2xpYi9kb3VnaG51dC1jaGFydC12YWx1ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSx5Q0FPQzs7O0lBTkMsa0RBQThCOztJQUM5QixrREFBOEI7O0lBQzlCLGdEQUE0Qjs7SUFDNUIseUNBQW1COztJQUNuQiwrQ0FBeUI7O0lBQ3pCLGdEQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgRG91Z2hudXRDaGFydFZhbHVlcyB7XHJcbiAgZG91Z2hudXRDaGFydENvbG9yczogc3RyaW5nW107XHJcbiAgZG91Z2hudXRDaGFydExhYmVsczogc3RyaW5nW107XHJcbiAgZG91Z2hudXRDaGFydERhdGE6IG51bWJlcltdO1xyXG4gIGNoYXJ0TGFiZWw6IHN0cmluZztcclxuICBpbnNpZGVDaGFydExhYmVsOiBzdHJpbmc7XHJcbiAgaW5zaWRlQ2hhcnROdW1iZXI6IG51bWJlcjtcclxufVxyXG4iXX0=
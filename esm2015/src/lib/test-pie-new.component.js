/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/test-pie-new.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { doughnutChartOptions, doughnutChartType } from './doughnut-chart';
import { TestPieNewService } from './test-pie-new.service';
export class TestPieNewComponent {
    /**
     * @param {?} pieChartService
     */
    constructor(pieChartService) {
        this.pieChartService = pieChartService;
        this.doughnutChartOptions = doughnutChartOptions;
        this.doughnutChartType = doughnutChartType;
        this.doughnutChartPlugins = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.doughnutChartColors = [
            { backgroundColor: this.doughnutChartValues.doughnutChartColors }
        ];
        this.addChartPlugin();
    }
    /**
     * @return {?}
     */
    addChartPlugin() {
        /** @type {?} */
        const component = this;
        this.doughnutChartPlugins = [
            {
                /**
                 * @param {?} chart
                 * @return {?}
                 */
                afterDraw(chart) {
                    /** @type {?} */
                    const ctx = chart.ctx;
                    /** @type {?} */
                    const insideChartNumber = component.pieChartService.formatNumber(component.doughnutChartValues.insideChartNumber);
                    /** @type {?} */
                    const insideChartText = component.doughnutChartValues.insideChartLabel;
                    chart.data.datasets.forEach((/**
                     * @param {?} dataset
                     * @return {?}
                     */
                    dataset => {
                        if (dataset.data.every((/**
                         * @param {?} element
                         * @return {?}
                         */
                        element => element === 0))) {
                            dataset.borderColor = '#fff';
                            dataset.borderWidth = 1;
                            dataset.backgroundColor = '#CCC';
                            dataset.data.push(1);
                        }
                    }));
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    /** @type {?} */
                    const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                    /** @type {?} */
                    const centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                    ctx.font = '20px HelveticaNeue';
                    ctx.fillStyle = 'black';
                    ctx.fillText('' + insideChartNumber, centerX, centerY - 10);
                    ctx.font = '13px HelveticaNeue';
                    ctx.fillText(insideChartText, centerX, centerY + 10);
                    ctx.restore();
                }
            }
        ];
    }
}
TestPieNewComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-test-pie-new',
                template: "<span class=\"no-wrap\">{{ doughnutChartValues.chartLabel }}</span>\r\n<div class=\"chart margin-right-S\">\r\n  <canvas\r\n    baseChart\r\n    [data]=\"doughnutChartValues.doughnutChartData\"\r\n    [labels]=\"doughnutChartValues.doughnutChartLabels\"\r\n    [colors]=\"doughnutChartColors\"\r\n    [chartType]=\"doughnutChartType\"\r\n    [options]=\"doughnutChartOptions\"\r\n    [plugins]=\"doughnutChartPlugins\"\r\n  ></canvas>\r\n</div>\r\n<div class=\"label-container\">\r\n  <div\r\n    class=\"chart-label\"\r\n    *ngFor=\"let label of doughnutChartValues.doughnutChartLabels; let i = index\"\r\n    [attr.data-index]=\"i\"\r\n  >\r\n    <span\r\n      class=\"dot\"\r\n      [ngStyle]=\"{\r\n        'background-color': doughnutChartValues.doughnutChartColors[i]\r\n      }\"\r\n    ></span>\r\n    <span>{{ label }} ({{ doughnutChartValues.doughnutChartData[i] }})</span>\r\n  </div>\r\n</div>\r\n"
            }] }
];
/** @nocollapse */
TestPieNewComponent.ctorParameters = () => [
    { type: TestPieNewService }
];
TestPieNewComponent.propDecorators = {
    doughnutChartValues: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartValues;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartColors;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartOptions;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartType;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartPlugins;
    /**
     * @type {?}
     * @private
     */
    TestPieNewComponent.prototype.pieChartService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1waWUtbmV3LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvdGVzdC1waWUtbmV3LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXpELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBTTNELE1BQU0sT0FBTyxtQkFBbUI7Ozs7SUFPOUIsWUFBb0IsZUFBa0M7UUFBbEMsb0JBQWUsR0FBZixlQUFlLENBQW1CO1FBSnRELHlCQUFvQixHQUFHLG9CQUFvQixDQUFDO1FBQzVDLHNCQUFpQixHQUFHLGlCQUFpQixDQUFDO1FBRXRDLHlCQUFvQixHQUFnRCxFQUFFLENBQUM7SUFDZCxDQUFDOzs7O0lBRTFELFFBQVEsS0FBSSxDQUFDOzs7O0lBRWIsV0FBVztRQUNULElBQUksQ0FBQyxtQkFBbUIsR0FBRztZQUN6QixFQUFFLGVBQWUsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLEVBQUU7U0FDbEUsQ0FBQztRQUVGLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7O0lBRUQsY0FBYzs7Y0FDTixTQUFTLEdBQUcsSUFBSTtRQUV0QixJQUFJLENBQUMsb0JBQW9CLEdBQUc7WUFDMUI7Ozs7O2dCQUNFLFNBQVMsQ0FBQyxLQUFZOzswQkFDZCxHQUFHLEdBQUcsS0FBSyxDQUFDLEdBQUc7OzBCQUNmLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUM5RCxTQUFTLENBQUMsbUJBQW1CLENBQUMsaUJBQWlCLENBQ2hEOzswQkFDSyxlQUFlLEdBQ25CLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0I7b0JBRWhELEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU87Ozs7b0JBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ3BDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLOzs7O3dCQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBQyxFQUFFOzRCQUNoRCxPQUFPLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQzs0QkFDN0IsT0FBTyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7NEJBQ3hCLE9BQU8sQ0FBQyxlQUFlLEdBQUcsTUFBTSxDQUFDOzRCQUNqQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt5QkFDdEI7b0JBQ0gsQ0FBQyxFQUFDLENBQUM7b0JBRUgsR0FBRyxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDOzswQkFDdEIsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzswQkFDNUQsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO29CQUVsRSxHQUFHLENBQUMsSUFBSSxHQUFHLG9CQUFvQixDQUFDO29CQUNoQyxHQUFHLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztvQkFDeEIsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQUcsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztvQkFFNUQsR0FBRyxDQUFDLElBQUksR0FBRyxvQkFBb0IsQ0FBQztvQkFDaEMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsT0FBTyxFQUFFLE9BQU8sR0FBRyxFQUFFLENBQUMsQ0FBQztvQkFFckQsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUNoQixDQUFDO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7O1lBOURGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QiwyNUJBQTRDO2FBRTdDOzs7O1lBTFEsaUJBQWlCOzs7a0NBT3ZCLEtBQUs7Ozs7SUFBTixrREFBa0Q7O0lBQ2xELGtEQUFvQjs7SUFDcEIsbURBQTRDOztJQUM1QyxnREFBc0M7O0lBRXRDLG1EQUF1RTs7Ozs7SUFDM0QsOENBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEb3VnaG51dENoYXJ0VmFsdWVzIH0gZnJvbSAnLi9kb3VnaG51dC1jaGFydC12YWx1ZXMnO1xuaW1wb3J0IHsgZG91Z2hudXRDaGFydE9wdGlvbnMsIGRvdWdobnV0Q2hhcnRUeXBlIH0gZnJvbSAnLi9kb3VnaG51dC1jaGFydCc7XG5pbXBvcnQgeyBQbHVnaW5TZXJ2aWNlR2xvYmFsUmVnaXN0cmF0aW9uQW5kT3B0aW9ucyB9IGZyb20gJ25nMi1jaGFydHMnO1xuaW1wb3J0IHsgVGVzdFBpZU5ld1NlcnZpY2UgfSBmcm9tICcuL3Rlc3QtcGllLW5ldy5zZXJ2aWNlJztcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi10ZXN0LXBpZS1uZXcnLFxuICB0ZW1wbGF0ZVVybDogJy4vdGVzdC1waWUtbmV3LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBUZXN0UGllTmV3Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgZG91Z2hudXRDaGFydFZhbHVlczogRG91Z2hudXRDaGFydFZhbHVlcztcbiAgZG91Z2hudXRDaGFydENvbG9ycztcbiAgZG91Z2hudXRDaGFydE9wdGlvbnMgPSBkb3VnaG51dENoYXJ0T3B0aW9ucztcbiAgZG91Z2hudXRDaGFydFR5cGUgPSBkb3VnaG51dENoYXJ0VHlwZTtcblxuICBkb3VnaG51dENoYXJ0UGx1Z2luczogUGx1Z2luU2VydmljZUdsb2JhbFJlZ2lzdHJhdGlvbkFuZE9wdGlvbnNbXSA9IFtdO1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHBpZUNoYXJ0U2VydmljZTogVGVzdFBpZU5ld1NlcnZpY2UpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIG5nT25DaGFuZ2VzKCkge1xuICAgIHRoaXMuZG91Z2hudXRDaGFydENvbG9ycyA9IFtcbiAgICAgIHsgYmFja2dyb3VuZENvbG9yOiB0aGlzLmRvdWdobnV0Q2hhcnRWYWx1ZXMuZG91Z2hudXRDaGFydENvbG9ycyB9XG4gICAgXTtcblxuICAgIHRoaXMuYWRkQ2hhcnRQbHVnaW4oKTtcbiAgfVxuXG4gIGFkZENoYXJ0UGx1Z2luKCkge1xuICAgIGNvbnN0IGNvbXBvbmVudCA9IHRoaXM7XG5cbiAgICB0aGlzLmRvdWdobnV0Q2hhcnRQbHVnaW5zID0gW1xuICAgICAge1xuICAgICAgICBhZnRlckRyYXcoY2hhcnQ6IENoYXJ0KSB7XG4gICAgICAgICAgY29uc3QgY3R4ID0gY2hhcnQuY3R4O1xuICAgICAgICAgIGNvbnN0IGluc2lkZUNoYXJ0TnVtYmVyID0gY29tcG9uZW50LnBpZUNoYXJ0U2VydmljZS5mb3JtYXROdW1iZXIoXG4gICAgICAgICAgICBjb21wb25lbnQuZG91Z2hudXRDaGFydFZhbHVlcy5pbnNpZGVDaGFydE51bWJlclxuICAgICAgICAgICk7XG4gICAgICAgICAgY29uc3QgaW5zaWRlQ2hhcnRUZXh0ID1cbiAgICAgICAgICAgIGNvbXBvbmVudC5kb3VnaG51dENoYXJ0VmFsdWVzLmluc2lkZUNoYXJ0TGFiZWw7XG5cbiAgICAgICAgICBjaGFydC5kYXRhLmRhdGFzZXRzLmZvckVhY2goZGF0YXNldCA9PiB7XG4gICAgICAgICAgICBpZiAoZGF0YXNldC5kYXRhLmV2ZXJ5KGVsZW1lbnQgPT4gZWxlbWVudCA9PT0gMCkpIHtcbiAgICAgICAgICAgICAgZGF0YXNldC5ib3JkZXJDb2xvciA9ICcjZmZmJztcbiAgICAgICAgICAgICAgZGF0YXNldC5ib3JkZXJXaWR0aCA9IDE7XG4gICAgICAgICAgICAgIGRhdGFzZXQuYmFja2dyb3VuZENvbG9yID0gJyNDQ0MnO1xuICAgICAgICAgICAgICBkYXRhc2V0LmRhdGEucHVzaCgxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcblxuICAgICAgICAgIGN0eC50ZXh0QWxpZ24gPSAnY2VudGVyJztcbiAgICAgICAgICBjdHgudGV4dEJhc2VsaW5lID0gJ21pZGRsZSc7XG4gICAgICAgICAgY29uc3QgY2VudGVyWCA9IChjaGFydC5jaGFydEFyZWEubGVmdCArIGNoYXJ0LmNoYXJ0QXJlYS5yaWdodCkgLyAyO1xuICAgICAgICAgIGNvbnN0IGNlbnRlclkgPSAoY2hhcnQuY2hhcnRBcmVhLnRvcCArIGNoYXJ0LmNoYXJ0QXJlYS5ib3R0b20pIC8gMjtcblxuICAgICAgICAgIGN0eC5mb250ID0gJzIwcHggSGVsdmV0aWNhTmV1ZSc7XG4gICAgICAgICAgY3R4LmZpbGxTdHlsZSA9ICdibGFjayc7XG4gICAgICAgICAgY3R4LmZpbGxUZXh0KCcnICsgaW5zaWRlQ2hhcnROdW1iZXIsIGNlbnRlclgsIGNlbnRlclkgLSAxMCk7XG5cbiAgICAgICAgICBjdHguZm9udCA9ICcxM3B4IEhlbHZldGljYU5ldWUnO1xuICAgICAgICAgIGN0eC5maWxsVGV4dChpbnNpZGVDaGFydFRleHQsIGNlbnRlclgsIGNlbnRlclkgKyAxMCk7XG5cbiAgICAgICAgICBjdHgucmVzdG9yZSgpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgXTtcbiAgfVxufVxuIl19
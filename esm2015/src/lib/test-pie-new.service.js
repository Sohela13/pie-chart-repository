/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/test-pie-new.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class TestPieNewService {
    constructor() { }
    /**
     * @return {?}
     */
    getChartValues() {
        /** @type {?} */
        const labels = [];
        /** @type {?} */
        const data = [100, 30];
        return {
            doughnutChartColors: ['#006652', '#eb8c00'],
            doughnutChartLabels: labels,
            doughnutChartData: data,
            chartLabel: '',
            insideChartLabel: 'Total hrs',
            insideChartNumber: 130
        };
    }
    /**
     * @param {?} num
     * @return {?}
     */
    formatNumber(num) {
        if (num < 10000) {
            return this.thousandFormatter(num.toString()); // e.g. 1234
        }
        else if (num < 1000000) {
            return (num / 1000).toFixed(0).toString() + 'K'; // e.g. 13K
        }
        else if (num < 10000000) {
            return (num / 1000000).toFixed(1).toString() + 'M'; // e.g. 1.3M
        }
        else {
            return (num / 1000000).toFixed(0).toString() + 'M'; // e.g. 13M
        }
    }
    /**
     * @param {?} number
     * @return {?}
     */
    thousandFormatter(number) {
        /** @type {?} */
        const num_parts = number.split('.');
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return num_parts.join('.');
    }
}
TestPieNewService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TestPieNewService.ctorParameters = () => [];
/** @nocollapse */ TestPieNewService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TestPieNewService_Factory() { return new TestPieNewService(); }, token: TestPieNewService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC1waWUtbmV3LnNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL3Rlc3QtcGllLW5ldy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFNM0MsTUFBTSxPQUFPLGlCQUFpQjtJQUM1QixnQkFBZSxDQUFDOzs7O0lBQ2hCLGNBQWM7O2NBQ04sTUFBTSxHQUFHLEVBQUU7O2NBQ1gsSUFBSSxHQUFhLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQztRQUNoQyxPQUFPO1lBQ0wsbUJBQW1CLEVBQUUsQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDO1lBQzNDLG1CQUFtQixFQUFFLE1BQU07WUFDM0IsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixVQUFVLEVBQUUsRUFBRTtZQUNkLGdCQUFnQixFQUFFLFdBQVc7WUFDN0IsaUJBQWlCLEVBQUUsR0FBRztTQUN2QixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsR0FBVztRQUN0QixJQUFJLEdBQUcsR0FBRyxLQUFLLEVBQUU7WUFDZixPQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDLFlBQVk7U0FDNUQ7YUFBTSxJQUFJLEdBQUcsR0FBRyxPQUFPLEVBQUU7WUFDeEIsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUMsV0FBVztTQUM3RDthQUFNLElBQUksR0FBRyxHQUFHLFFBQVEsRUFBRTtZQUN6QixPQUFPLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxZQUFZO1NBQ2pFO2FBQU07WUFDTCxPQUFPLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHLENBQUMsQ0FBQyxXQUFXO1NBQ2hFO0lBQ0gsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxNQUFjOztjQUN4QixTQUFTLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDbkMsU0FBUyxDQUFDLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDbEUsT0FBTyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzdCLENBQUM7OztZQWxDRixVQUFVLFNBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEb3VnaG51dENoYXJ0VmFsdWVzIH0gZnJvbSAnLi9kb3VnaG51dC1jaGFydC12YWx1ZXMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBUZXN0UGllTmV3U2VydmljZSB7XG4gIGNvbnN0cnVjdG9yKCkge31cbiAgZ2V0Q2hhcnRWYWx1ZXMoKTogRG91Z2hudXRDaGFydFZhbHVlcyB7XG4gICAgY29uc3QgbGFiZWxzID0gW107XG4gICAgY29uc3QgZGF0YTogbnVtYmVyW10gPSBbMTAwLCAzMF07XG4gICAgcmV0dXJuIHtcbiAgICAgIGRvdWdobnV0Q2hhcnRDb2xvcnM6IFsnIzAwNjY1MicsICcjZWI4YzAwJ10sXG4gICAgICBkb3VnaG51dENoYXJ0TGFiZWxzOiBsYWJlbHMsXG4gICAgICBkb3VnaG51dENoYXJ0RGF0YTogZGF0YSxcbiAgICAgIGNoYXJ0TGFiZWw6ICcnLFxuICAgICAgaW5zaWRlQ2hhcnRMYWJlbDogJ1RvdGFsIGhycycsXG4gICAgICBpbnNpZGVDaGFydE51bWJlcjogMTMwXG4gICAgfTtcbiAgfVxuXG4gIGZvcm1hdE51bWJlcihudW06IG51bWJlcik6IHN0cmluZyB7XG4gICAgaWYgKG51bSA8IDEwMDAwKSB7XG4gICAgICByZXR1cm4gdGhpcy50aG91c2FuZEZvcm1hdHRlcihudW0udG9TdHJpbmcoKSk7IC8vIGUuZy4gMTIzNFxuICAgIH0gZWxzZSBpZiAobnVtIDwgMTAwMDAwMCkge1xuICAgICAgcmV0dXJuIChudW0gLyAxMDAwKS50b0ZpeGVkKDApLnRvU3RyaW5nKCkgKyAnSyc7IC8vIGUuZy4gMTNLXG4gICAgfSBlbHNlIGlmIChudW0gPCAxMDAwMDAwMCkge1xuICAgICAgcmV0dXJuIChudW0gLyAxMDAwMDAwKS50b0ZpeGVkKDEpLnRvU3RyaW5nKCkgKyAnTSc7IC8vIGUuZy4gMS4zTVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gKG51bSAvIDEwMDAwMDApLnRvRml4ZWQoMCkudG9TdHJpbmcoKSArICdNJzsgLy8gZS5nLiAxM01cbiAgICB9XG4gIH1cblxuICB0aG91c2FuZEZvcm1hdHRlcihudW1iZXI6IHN0cmluZykge1xuICAgIGNvbnN0IG51bV9wYXJ0cyA9IG51bWJlci5zcGxpdCgnLicpO1xuICAgIG51bV9wYXJ0c1swXSA9IG51bV9wYXJ0c1swXS5yZXBsYWNlKC9cXEIoPz0oXFxkezN9KSsoPyFcXGQpKS9nLCAnLCcpO1xuICAgIHJldHVybiBudW1fcGFydHMuam9pbignLicpO1xuICB9XG59XG4iXX0=
/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/doughnut-chart.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export const doughnutChartColors = [
    {
        backgroundColor: [
            "#00C6F7",
            "#7D7D7D",
            "#295477",
            "#299D8F",
            "#EB8C00",
            "#D04A02",
            "#0A1319"
        ]
    }
];
/** @type {?} */
export const doughnutChartOptions = {
    cutoutPercentage: 60,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
        display: false
    },
    tooltips: {
        enabled: false
    }
};
/** @type {?} */
export const doughnutChartType = "doughnut";
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG91Z2hudXQtY2hhcnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL2RvdWdobnV0LWNoYXJ0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUdBLE1BQU0sT0FBTyxtQkFBbUIsR0FBWTtJQUMxQztRQUNFLGVBQWUsRUFBRTtZQUNmLFNBQVM7WUFDVCxTQUFTO1lBQ1QsU0FBUztZQUNULFNBQVM7WUFDVCxTQUFTO1lBQ1QsU0FBUztZQUNULFNBQVM7U0FDVjtLQUNGO0NBQ0Y7O0FBRUQsTUFBTSxPQUFPLG9CQUFvQixHQUFpQjtJQUNoRCxnQkFBZ0IsRUFBRSxFQUFFO0lBQ3BCLFVBQVUsRUFBRSxJQUFJO0lBQ2hCLG1CQUFtQixFQUFFLEtBQUs7SUFDMUIsTUFBTSxFQUFFO1FBQ04sT0FBTyxFQUFFLEtBQUs7S0FDZjtJQUNELFFBQVEsRUFBRTtRQUNSLE9BQU8sRUFBRSxLQUFLO0tBQ2Y7Q0FDRjs7QUFFRCxNQUFNLE9BQU8saUJBQWlCLEdBQWMsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENoYXJ0T3B0aW9ucywgQ2hhcnRUeXBlIH0gZnJvbSBcImNoYXJ0LmpzXCI7XHJcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcIm5nMi1jaGFydHNcIjtcclxuXHJcbmV4cG9ydCBjb25zdCBkb3VnaG51dENoYXJ0Q29sb3JzOiBDb2xvcltdID0gW1xyXG4gIHtcclxuICAgIGJhY2tncm91bmRDb2xvcjogW1xyXG4gICAgICBcIiMwMEM2RjdcIixcclxuICAgICAgXCIjN0Q3RDdEXCIsXHJcbiAgICAgIFwiIzI5NTQ3N1wiLFxyXG4gICAgICBcIiMyOTlEOEZcIixcclxuICAgICAgXCIjRUI4QzAwXCIsXHJcbiAgICAgIFwiI0QwNEEwMlwiLFxyXG4gICAgICBcIiMwQTEzMTlcIlxyXG4gICAgXVxyXG4gIH1cclxuXTtcclxuXHJcbmV4cG9ydCBjb25zdCBkb3VnaG51dENoYXJ0T3B0aW9uczogQ2hhcnRPcHRpb25zID0ge1xyXG4gIGN1dG91dFBlcmNlbnRhZ2U6IDYwLFxyXG4gIHJlc3BvbnNpdmU6IHRydWUsXHJcbiAgbWFpbnRhaW5Bc3BlY3RSYXRpbzogZmFsc2UsXHJcbiAgbGVnZW5kOiB7XHJcbiAgICBkaXNwbGF5OiBmYWxzZVxyXG4gIH0sXHJcbiAgdG9vbHRpcHM6IHtcclxuICAgIGVuYWJsZWQ6IGZhbHNlXHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGNvbnN0IGRvdWdobnV0Q2hhcnRUeXBlOiBDaGFydFR5cGUgPSBcImRvdWdobnV0XCI7XHJcbiJdfQ==
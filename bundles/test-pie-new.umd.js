(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('ng2-charts')) :
    typeof define === 'function' && define.amd ? define('test-pie-new', ['exports', '@angular/core', '@angular/common', 'ng2-charts'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['test-pie-new'] = {}, global.ng.core, global.ng.common, global.ng2Charts));
}(this, (function (exports, i0, common, ng2Charts) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * Generated from: src/lib/test-pie-new.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TestPieNewService = /** @class */ (function () {
        function TestPieNewService() {
        }
        /**
         * @return {?}
         */
        TestPieNewService.prototype.getChartValues = function () {
            /** @type {?} */
            var labels = [];
            /** @type {?} */
            var data = [100, 30];
            return {
                doughnutChartColors: ['#006652', '#eb8c00'],
                doughnutChartLabels: labels,
                doughnutChartData: data,
                chartLabel: '',
                insideChartLabel: 'Total hrs',
                insideChartNumber: 130
            };
        };
        /**
         * @param {?} num
         * @return {?}
         */
        TestPieNewService.prototype.formatNumber = function (num) {
            if (num < 10000) {
                return this.thousandFormatter(num.toString()); // e.g. 1234
            }
            else if (num < 1000000) {
                return (num / 1000).toFixed(0).toString() + 'K'; // e.g. 13K
            }
            else if (num < 10000000) {
                return (num / 1000000).toFixed(1).toString() + 'M'; // e.g. 1.3M
            }
            else {
                return (num / 1000000).toFixed(0).toString() + 'M'; // e.g. 13M
            }
        };
        /**
         * @param {?} number
         * @return {?}
         */
        TestPieNewService.prototype.thousandFormatter = function (number) {
            /** @type {?} */
            var num_parts = number.split('.');
            num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            return num_parts.join('.');
        };
        return TestPieNewService;
    }());
    TestPieNewService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TestPieNewService.ctorParameters = function () { return []; };
    /** @nocollapse */ TestPieNewService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TestPieNewService_Factory() { return new TestPieNewService(); }, token: TestPieNewService, providedIn: "root" });

    /**
     * @fileoverview added by tsickle
     * Generated from: src/lib/doughnut-chart.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var doughnutChartColors = [
        {
            backgroundColor: [
                "#00C6F7",
                "#7D7D7D",
                "#295477",
                "#299D8F",
                "#EB8C00",
                "#D04A02",
                "#0A1319"
            ]
        }
    ];
    /** @type {?} */
    var doughnutChartOptions = {
        cutoutPercentage: 60,
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    };
    /** @type {?} */
    var doughnutChartType = "doughnut";

    /**
     * @fileoverview added by tsickle
     * Generated from: src/lib/test-pie-new.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TestPieNewComponent = /** @class */ (function () {
        /**
         * @param {?} pieChartService
         */
        function TestPieNewComponent(pieChartService) {
            this.pieChartService = pieChartService;
            this.doughnutChartOptions = doughnutChartOptions;
            this.doughnutChartType = doughnutChartType;
            this.doughnutChartPlugins = [];
        }
        /**
         * @return {?}
         */
        TestPieNewComponent.prototype.ngOnInit = function () { };
        /**
         * @return {?}
         */
        TestPieNewComponent.prototype.ngOnChanges = function () {
            this.doughnutChartColors = [
                { backgroundColor: this.doughnutChartValues.doughnutChartColors }
            ];
            this.addChartPlugin();
        };
        /**
         * @return {?}
         */
        TestPieNewComponent.prototype.addChartPlugin = function () {
            /** @type {?} */
            var component = this;
            this.doughnutChartPlugins = [
                {
                    /**
                     * @param {?} chart
                     * @return {?}
                     */
                    afterDraw: function (chart) {
                        /** @type {?} */
                        var ctx = chart.ctx;
                        /** @type {?} */
                        var insideChartNumber = component.pieChartService.formatNumber(component.doughnutChartValues.insideChartNumber);
                        /** @type {?} */
                        var insideChartText = component.doughnutChartValues.insideChartLabel;
                        chart.data.datasets.forEach(( /**
                         * @param {?} dataset
                         * @return {?}
                         */function (/**
                         * @param {?} dataset
                         * @return {?}
                         */ dataset) {
                            if (dataset.data.every(( /**
                             * @param {?} element
                             * @return {?}
                             */function (/**
                             * @param {?} element
                             * @return {?}
                             */ element) { return element === 0; }))) {
                                dataset.borderColor = '#fff';
                                dataset.borderWidth = 1;
                                dataset.backgroundColor = '#CCC';
                                dataset.data.push(1);
                            }
                        }));
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';
                        /** @type {?} */
                        var centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                        /** @type {?} */
                        var centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                        ctx.font = '20px HelveticaNeue';
                        ctx.fillStyle = 'black';
                        ctx.fillText('' + insideChartNumber, centerX, centerY - 10);
                        ctx.font = '13px HelveticaNeue';
                        ctx.fillText(insideChartText, centerX, centerY + 10);
                        ctx.restore();
                    }
                }
            ];
        };
        return TestPieNewComponent;
    }());
    TestPieNewComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'lib-test-pie-new',
                    template: "<span class=\"no-wrap\">{{ doughnutChartValues.chartLabel }}</span>\r\n<div class=\"chart margin-right-S\">\r\n  <canvas\r\n    baseChart\r\n    [data]=\"doughnutChartValues.doughnutChartData\"\r\n    [labels]=\"doughnutChartValues.doughnutChartLabels\"\r\n    [colors]=\"doughnutChartColors\"\r\n    [chartType]=\"doughnutChartType\"\r\n    [options]=\"doughnutChartOptions\"\r\n    [plugins]=\"doughnutChartPlugins\"\r\n  ></canvas>\r\n</div>\r\n<div class=\"label-container\">\r\n  <div\r\n    class=\"chart-label\"\r\n    *ngFor=\"let label of doughnutChartValues.doughnutChartLabels; let i = index\"\r\n    [attr.data-index]=\"i\"\r\n  >\r\n    <span\r\n      class=\"dot\"\r\n      [ngStyle]=\"{\r\n        'background-color': doughnutChartValues.doughnutChartColors[i]\r\n      }\"\r\n    ></span>\r\n    <span>{{ label }} ({{ doughnutChartValues.doughnutChartData[i] }})</span>\r\n  </div>\r\n</div>\r\n"
                }] }
    ];
    /** @nocollapse */
    TestPieNewComponent.ctorParameters = function () { return [
        { type: TestPieNewService }
    ]; };
    TestPieNewComponent.propDecorators = {
        doughnutChartValues: [{ type: i0.Input }]
    };
    if (false) {
        /** @type {?} */
        TestPieNewComponent.prototype.doughnutChartValues;
        /** @type {?} */
        TestPieNewComponent.prototype.doughnutChartColors;
        /** @type {?} */
        TestPieNewComponent.prototype.doughnutChartOptions;
        /** @type {?} */
        TestPieNewComponent.prototype.doughnutChartType;
        /** @type {?} */
        TestPieNewComponent.prototype.doughnutChartPlugins;
        /**
         * @type {?}
         * @private
         */
        TestPieNewComponent.prototype.pieChartService;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: src/lib/test-pie-new.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TestPieNewModule = /** @class */ (function () {
        function TestPieNewModule() {
        }
        return TestPieNewModule;
    }());
    TestPieNewModule.decorators = [
        { type: i0.NgModule, args: [{
                    declarations: [TestPieNewComponent],
                    imports: [common.CommonModule, ng2Charts.ChartsModule],
                    exports: [TestPieNewComponent, common.CommonModule, ng2Charts.ChartsModule]
                },] }
    ];

    /**
     * @fileoverview added by tsickle
     * Generated from: public_api.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * Generated from: test-pie-new.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.TestPieNewComponent = TestPieNewComponent;
    exports.TestPieNewModule = TestPieNewModule;
    exports.TestPieNewService = TestPieNewService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=test-pie-new.umd.js.map

import { Injectable, ɵɵdefineInjectable, Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/test-pie-new.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TestPieNewService {
    constructor() { }
    /**
     * @return {?}
     */
    getChartValues() {
        /** @type {?} */
        const labels = [];
        /** @type {?} */
        const data = [100, 30];
        return {
            doughnutChartColors: ['#006652', '#eb8c00'],
            doughnutChartLabels: labels,
            doughnutChartData: data,
            chartLabel: '',
            insideChartLabel: 'Total hrs',
            insideChartNumber: 130
        };
    }
    /**
     * @param {?} num
     * @return {?}
     */
    formatNumber(num) {
        if (num < 10000) {
            return this.thousandFormatter(num.toString()); // e.g. 1234
        }
        else if (num < 1000000) {
            return (num / 1000).toFixed(0).toString() + 'K'; // e.g. 13K
        }
        else if (num < 10000000) {
            return (num / 1000000).toFixed(1).toString() + 'M'; // e.g. 1.3M
        }
        else {
            return (num / 1000000).toFixed(0).toString() + 'M'; // e.g. 13M
        }
    }
    /**
     * @param {?} number
     * @return {?}
     */
    thousandFormatter(number) {
        /** @type {?} */
        const num_parts = number.split('.');
        num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return num_parts.join('.');
    }
}
TestPieNewService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TestPieNewService.ctorParameters = () => [];
/** @nocollapse */ TestPieNewService.ngInjectableDef = ɵɵdefineInjectable({ factory: function TestPieNewService_Factory() { return new TestPieNewService(); }, token: TestPieNewService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/doughnut-chart.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const doughnutChartColors = [
    {
        backgroundColor: [
            "#00C6F7",
            "#7D7D7D",
            "#295477",
            "#299D8F",
            "#EB8C00",
            "#D04A02",
            "#0A1319"
        ]
    }
];
/** @type {?} */
const doughnutChartOptions = {
    cutoutPercentage: 60,
    responsive: true,
    maintainAspectRatio: false,
    legend: {
        display: false
    },
    tooltips: {
        enabled: false
    }
};
/** @type {?} */
const doughnutChartType = "doughnut";

/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/test-pie-new.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TestPieNewComponent {
    /**
     * @param {?} pieChartService
     */
    constructor(pieChartService) {
        this.pieChartService = pieChartService;
        this.doughnutChartOptions = doughnutChartOptions;
        this.doughnutChartType = doughnutChartType;
        this.doughnutChartPlugins = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.doughnutChartColors = [
            { backgroundColor: this.doughnutChartValues.doughnutChartColors }
        ];
        this.addChartPlugin();
    }
    /**
     * @return {?}
     */
    addChartPlugin() {
        /** @type {?} */
        const component = this;
        this.doughnutChartPlugins = [
            {
                /**
                 * @param {?} chart
                 * @return {?}
                 */
                afterDraw(chart) {
                    /** @type {?} */
                    const ctx = chart.ctx;
                    /** @type {?} */
                    const insideChartNumber = component.pieChartService.formatNumber(component.doughnutChartValues.insideChartNumber);
                    /** @type {?} */
                    const insideChartText = component.doughnutChartValues.insideChartLabel;
                    chart.data.datasets.forEach((/**
                     * @param {?} dataset
                     * @return {?}
                     */
                    dataset => {
                        if (dataset.data.every((/**
                         * @param {?} element
                         * @return {?}
                         */
                        element => element === 0))) {
                            dataset.borderColor = '#fff';
                            dataset.borderWidth = 1;
                            dataset.backgroundColor = '#CCC';
                            dataset.data.push(1);
                        }
                    }));
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    /** @type {?} */
                    const centerX = (chart.chartArea.left + chart.chartArea.right) / 2;
                    /** @type {?} */
                    const centerY = (chart.chartArea.top + chart.chartArea.bottom) / 2;
                    ctx.font = '20px HelveticaNeue';
                    ctx.fillStyle = 'black';
                    ctx.fillText('' + insideChartNumber, centerX, centerY - 10);
                    ctx.font = '13px HelveticaNeue';
                    ctx.fillText(insideChartText, centerX, centerY + 10);
                    ctx.restore();
                }
            }
        ];
    }
}
TestPieNewComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-test-pie-new',
                template: "<span class=\"no-wrap\">{{ doughnutChartValues.chartLabel }}</span>\r\n<div class=\"chart margin-right-S\">\r\n  <canvas\r\n    baseChart\r\n    [data]=\"doughnutChartValues.doughnutChartData\"\r\n    [labels]=\"doughnutChartValues.doughnutChartLabels\"\r\n    [colors]=\"doughnutChartColors\"\r\n    [chartType]=\"doughnutChartType\"\r\n    [options]=\"doughnutChartOptions\"\r\n    [plugins]=\"doughnutChartPlugins\"\r\n  ></canvas>\r\n</div>\r\n<div class=\"label-container\">\r\n  <div\r\n    class=\"chart-label\"\r\n    *ngFor=\"let label of doughnutChartValues.doughnutChartLabels; let i = index\"\r\n    [attr.data-index]=\"i\"\r\n  >\r\n    <span\r\n      class=\"dot\"\r\n      [ngStyle]=\"{\r\n        'background-color': doughnutChartValues.doughnutChartColors[i]\r\n      }\"\r\n    ></span>\r\n    <span>{{ label }} ({{ doughnutChartValues.doughnutChartData[i] }})</span>\r\n  </div>\r\n</div>\r\n"
            }] }
];
/** @nocollapse */
TestPieNewComponent.ctorParameters = () => [
    { type: TestPieNewService }
];
TestPieNewComponent.propDecorators = {
    doughnutChartValues: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartValues;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartColors;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartOptions;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartType;
    /** @type {?} */
    TestPieNewComponent.prototype.doughnutChartPlugins;
    /**
     * @type {?}
     * @private
     */
    TestPieNewComponent.prototype.pieChartService;
}

/**
 * @fileoverview added by tsickle
 * Generated from: src/lib/test-pie-new.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TestPieNewModule {
}
TestPieNewModule.decorators = [
    { type: NgModule, args: [{
                declarations: [TestPieNewComponent],
                imports: [CommonModule, ChartsModule],
                exports: [TestPieNewComponent, CommonModule, ChartsModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public_api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: test-pie-new.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { TestPieNewComponent, TestPieNewModule, TestPieNewService };
//# sourceMappingURL=test-pie-new.js.map

import { OnInit } from '@angular/core';
import { DoughnutChartValues } from './doughnut-chart-values';
import { PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';
import { TestPieNewService } from './test-pie-new.service';
export declare class TestPieNewComponent implements OnInit {
    private pieChartService;
    doughnutChartValues: DoughnutChartValues;
    doughnutChartColors: any;
    doughnutChartOptions: import("chart.js").ChartOptions;
    doughnutChartType: import("chart.js").ChartType;
    doughnutChartPlugins: PluginServiceGlobalRegistrationAndOptions[];
    constructor(pieChartService: TestPieNewService);
    ngOnInit(): void;
    ngOnChanges(): void;
    addChartPlugin(): void;
}

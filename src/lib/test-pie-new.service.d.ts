import { DoughnutChartValues } from './doughnut-chart-values';
export declare class TestPieNewService {
    constructor();
    getChartValues(): DoughnutChartValues;
    formatNumber(num: number): string;
    thousandFormatter(number: string): string;
}

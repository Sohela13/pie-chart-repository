export interface DoughnutChartValues {
    doughnutChartColors: string[];
    doughnutChartLabels: string[];
    doughnutChartData: number[];
    chartLabel: string;
    insideChartLabel: string;
    insideChartNumber: number;
}

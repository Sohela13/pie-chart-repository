import { ChartOptions, ChartType } from "chart.js";
import { Color } from "ng2-charts";
export declare const doughnutChartColors: Color[];
export declare const doughnutChartOptions: ChartOptions;
export declare const doughnutChartType: ChartType;
